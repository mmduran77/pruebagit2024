package ar.edu.unju.fi.demoGit.model;

import org.springframework.stereotype.Component;

@Component
public class Compras {
	private int IdCompra;
	private Producto producto;
	private int cantidad;
	
	public Compras() {
		// TODO Auto-generated constructor stub
	}

	public Compras(int idCompra, Producto producto, int cantidad) {
		super();
		IdCompra = idCompra;
		this.producto = producto;
		this.cantidad = cantidad;
	}

	public int getIdCompra() {
		return IdCompra;
	}

	public void setIdCompra(int idCompra) {
		IdCompra = idCompra;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
	
}
